# iPaste

iPaste is an application to write or paste notes and documents which will be saved on IPFS. Currently we work by an IPFS gateway. In the future your browser might needs to support IPFS. Brave and Opera can do this by default. Some other browsers might have extensions for IPFS available by https://github.com/ipfs/ipfs-companion#ipfs-companion

# Free Open Source

Completely free open source MIT licensed code that can be audited and extended by anyone.

# Client side

iPaste runs mostly client side. Currently data is saved at an IPFS gateway.

# Development

Currently iPaste is under development for its first Proof of Concept (PoC). Future plans are to decentralise the development by making it easy to fork and use a fork by version compatibility via version management in a JSON field.

The code is developed W3C and WCAG validated and secure.

# Data / Privacy
The data entered in the note is basically saved unencrypted by IPFS. Currently the app use the https://infura.io/ IPFS gateway

# Organisation

Currently iPaste is developed by [Iftakhar Hasnayen Abir](https://gitlab.com/ih-abir) with [Jurjen de Vries](https://gitlab.com/jurjendevries) as product owner. Partly used as a study to Web3, decentralized and distributed development. In the future the organization might be transformed into a decentralized autonomous organization (DAO).

# Todo

The todo list of iPaste is currently maintained by a public Wekan agile scrum board https://wekan.foss.casa/b/HHHokNwsF655nWjhJ/ipaste-sprint

# App documentation

## To view saved note 

	Add ?CID to the URL where iPaste is hosted
	e.g. https://ipaste-cc.gitlab.io/ipaste/?QmP2bhRNxpyCHj25xjeWNJdiEVrMgkTn5SioquecKjkFUz
